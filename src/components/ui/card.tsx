import * as React from "react"

import { cn } from "@/lib/utils"
import {buttonVariants} from "@/components/ui/button";
import {ArrowUpRight} from "lucide-react";
import type {VariantProps} from "class-variance-authority";

const Card = React.forwardRef<
  HTMLDivElement,
  React.HTMLAttributes<HTMLDivElement>
>(({ className, ...props }, ref) => (
  <div
    ref={ref}
    className={cn(
      "rounded-lg border bg-card text-card-foreground shadow-sm",
      className
    )}
    {...props}
  />
))
Card.displayName = "Card"

const CardHeader = React.forwardRef<
  HTMLDivElement,
  React.HTMLAttributes<HTMLDivElement>
>(({ className, ...props }, ref) => (
  <div
    ref={ref}
    className={cn("flex flex-col space-y-1.5 p-6", className)}
    {...props}
  />
))
CardHeader.displayName = "CardHeader"

const CardTitle = React.forwardRef<
  HTMLParagraphElement,
  React.HTMLAttributes<HTMLHeadingElement>
>(({ className, ...props }, ref) => (
  <h3
    ref={ref}
    className={cn(
      "text-2xl font-semibold leading-none tracking-tight",
      className
    )}
    {...props}
  />
))
CardTitle.displayName = "CardTitle"

const CardDescription = React.forwardRef<
  HTMLParagraphElement,
  React.HTMLAttributes<HTMLParagraphElement>
>(({ className, ...props }, ref) => (
  <p
    ref={ref}
    className={cn("text-sm text-muted-foreground", className)}
    {...props}
  />
))
CardDescription.displayName = "CardDescription"

const CardContent = React.forwardRef<
  HTMLDivElement,
  React.HTMLAttributes<HTMLDivElement>
>(({ className, ...props }, ref) => (
  <div ref={ref} className={cn("p-6 pt-0", className)} {...props} />
))
CardContent.displayName = "CardContent"

const CardFooter = React.forwardRef<
  HTMLDivElement,
  React.HTMLAttributes<HTMLDivElement>
>(({ className, ...props }, ref) => (
  <div
    ref={ref}
    className={cn(" flex items-center justify-end p-6 pt-0", className)}
    {...props}
  />
))
CardFooter.displayName = "CardFooter"

export interface ButtonProps
    extends React.AnchorHTMLAttributes<HTMLAnchorElement>,
        VariantProps<typeof buttonVariants> {
    url?: string
}

const CardButton = React.forwardRef<
    HTMLAnchorElement, ButtonProps
>(({ url = '', ...props }, ref) => (
    <>
        <a href={url}
           className={buttonVariants({ variant: "outline" })}
           target="_blank"
           rel="nofollow noopener"
           {...props}
        >
            {props.children}
            <ArrowUpRight className="ml-2 h-4 w-4 text-muted-foreground" />
        </a>
    </>
))
CardButton.displayName = "CardButton"

export { Card, CardHeader, CardFooter, CardTitle, CardDescription, CardContent, CardButton }
