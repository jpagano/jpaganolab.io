import { defineConfig } from 'astro/config';
import tailwind from "@astrojs/tailwind";

import react from "@astrojs/react";

// https://astro.build/config
export default defineConfig({
  base: '',
  outDir: 'public',
  publicDir: 'static',
  site: 'https://jpagano.gitlab.io',
  integrations: [tailwind({
    applyBaseStyles: false,
  }), react()]
});
